<?php


namespace App\Tests;


use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductTest extends KernelTestCase
{
    /**
     * @test
     */
    public function testShouldProductHaveNoStock()
    {
        $expected = null;

        $product = new Product();

        $this->assertEquals($expected, $product->getStock());
    }

    /**
     * @test
     */
    public function testShouldProductHaveStock()
    {
        $expected = Stock::class;

        $product = new Product();
        $product->setStock(new Stock());
        $this->assertInstanceOf($expected, $product->getStock());
    }

}